
//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");
function changeRadii() {
    let slider = document.querySelector("#rangeR");
    let circle1 = document.querySelector("#circleleft");
    let circle2 = document.querySelector("#circleright");
    let r2 = 200 - Number(slider.value);
    console.log(circle1);
    console.log(circle2);
    circle1.setAttribute("r", slider.value);
    circle2.setAttribute("r", r2);
}
window.changeRadii = changeRadii;
function changePupilX() {
    let slider = document.querySelector("#rangeX");
    let pupil1 = document.querySelector("#leftpupil");
    let pupil2 = document.querySelector("#rightpupil");
    console.log(pupil1);
    console.log(pupil2);
    console.log(slider.value);
    pupil1.setAttribute("cx", 170 + Number(slider.value));
    pupil2.setAttribute("cx", 270 + Number(slider.value));
}
window.changePupilX = changePupilX;
function changePupilY() {
    let slider = document.querySelector("#rangeY");
    let pupil1 = document.querySelector("#leftpupil");
    let pupil2 = document.querySelector("#rightpupil");
    console.log(pupil1);
    console.log(pupil2);
    pupil1.setAttribute("cy", 115 + Number(slider.value));
    pupil2.setAttribute("cy", 115 + Number(slider.value))
}
window.changePupilY = changePupilY;
function changeSmileY() {
    let slider = document.querySelector("#smile");
    let smile1 = document.querySelector("#smile1");
    let smile2 = document.querySelector("#smile2");
    console.log(smile1);
    console.log(smile2);
    smile1.setAttribute("y1", 190 + Number(slider.value));
    smile2.setAttribute("y2", 190 + Number(slider.value))
}
window.changeSmileY = changeSmileY;

function changeHairColor() {
    let colorPicker = document.querySelector("#colorpicker");
    let hairs = document.querySelector("#hairs");
    hairs.setAttribute("fill", colorPicker.value);
    console.log(hairs);
    console.log(colorPicker.value);
}
window.changeHairColor = changeHairColor;

function changeHairLength() {
    let hairs = document.querySelector("#hairs");
    let length = document.querySelector("#hairSize");
    let x = length.value;
    x = Number(x);
    hairs.setAttribute("points", ((100 - x) + "," + (100 + x) + " 150,30 170,10 250,5 300,10 350,30 " + (380 + x) + "," + (100 + x) + " 310,85 220,60 190," + (70 + x) + " 170,50"));
    console.log(hairs);
}
window.changeHairLength = changeHairLength; 