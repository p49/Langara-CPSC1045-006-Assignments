//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");
function drawBoxes() {
    let boxes = document.querySelector("#number");
    let grow = document.querySelector("#growth");
    let size = document.querySelector("#size");
    var num = Number(boxes.value);
    var inc = Number(grow.value);
    var siz = Number(size.value);
    var position = 0;
    let svgString = '<svg height="1000" width="5000">';
    for (i = 1; i <= num; i++) {
        addingData();
    }
    function addingData() {
        boxString = '<rect x="' + position + '" y="' + 50 + '" width="' + siz + '" height="' + siz + '"></rect>';
        svgString += boxString;
        siz = siz + inc;
        position += siz;
    }

    svgString += '</svg>';
    let output = document.querySelector("#output");
    output.innerHTML = svgString;
    console.log(svgString);
    }
window.drawBoxes = drawBoxes;