let baseString = "TestPage"
function populatePage() {
    let containers = document.querySelectorAll(".container"); //get all the containers

    //Part 1: Rewrite the following code using a for/of loop. 
    //Trivial example: This code capitalizes what is in the HTML file
    //and counts the number of charcters
    //reference for/of loops: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...of
    //reference for loops: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for
    console.log(containers.length);

    containers //all the element 
    containers[0].innerHTML.toUpperCase();
    let count = 0;
    for(let i =0; i < containers.length; i++){
        console.log(count);
        containers[i].innerHTML=getUpperCase(containers[i]);
        //do something with count 
        count = count + Number(containers.length);
       
    }
    


    //Output Part 1: You do not need to change this.
    let output = document.querySelector("#output");
    output.innerHTML = "Number of characters:" + count;

    //Part 2: Rewrite the following using a loop and a function
    let heights = [4, 5, 3, 7, 6, 10,-2];
    let pointString = "";//start with an empy string
    let x = 0;
    let y=0;
    for(i=0;i<heights.length;i++){
        x=x+10;
       definePoints(x,y,heights[i]);
    }
    

    //Replace the following code with a for/of loop.
    //In this loop, call a function that you will write to 
    //help create the string of points
function definePoints(x,y,h){
    y=200-h*20;
    pointString=pointString+x+","+y+" ";
}console.log(pointString);
    
    //Take the next line of code and make it a function
    //You function will take in, x, y, and the existing string ( 3 parameters into total)
    //and return the new string with the ponts added.
    //Reference: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions

    let output2 = document.querySelector("#output2");
    output2.setAttribute("points", pointString);


    let newThings = []
    //Part 3:  Creating things with loop
    // on the html page with loop.
    //mananually, there are other ways of course.
    let data = [30, 70, 110, 150];
    let svgString = '<svg width="500" height="500">';
    x= 0;
    for(i=0;i<data.length;i++){
        x=data[i];
        cx=x;
        cy=250;
        r=20;
        fillcolor="black";
        CircleCode(cx,cy,r,fillcolor);
    }
    function CircleCode(cx,cy,r,fill){
        circleString='<circle cx="'+x+'"'+' cy="'+cy+'" r="'+r+'" fill=" black"/>';
        svgString +=circleString;
    }
    console.log(svgString);


    svgString += "</svg>"
    let output3 = document.querySelector("#output3")
    document.body.innerHTML += svgString;

}


/**
 * this function takes a parameter and returns the uppercase of this parameter
 */
function getUpperCase(element) {
    return element.innerHTML.toUpperCase();
}

