/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");
function update(){
    let xm=document.querySelector("#xaxis");
    let ym=document.querySelector("#yaxis");
    let slider=document.querySelector("#angle");
    let z=document.querySelector("#collection");
    var a=Number(xm.value);
    var b=Number(ym.value);
    var c=Number(slider.value);
    var translatestring="translate("+a+","+b+")";
    console.log(translatestring);
    var rotatestring="rotate("+c+")";
    console.log(rotatestring);
    var transformstring=translatestring+" "+rotatestring;
    console.log(transformstring);
    z.setAttribute("transform",transformstring);
    }
window.update=update;
function Starsize(){
    let Spoints=document.querySelector("#group");
    let or=document.querySelector("#outerR");
    let ir=document.querySelector("#innerR");
    let xm=document.querySelector("#xax");
    let ym=document.querySelector("#yax");
    var x=Number(or.value);
    var y=Number(ir.value);
    var z=Number(Spoints);
    var xx=Number(xm.value);
    var yx=Number(ym.value);
    console.log(x);
    console.log(y);
    var a=Math.sin(0);
    var b=Math.sin(0.76);
    var c=Math.sin(1.57);
    var d=Math.sin(2.36);
    var e=Math.sin(3.14);
    var f=Math.sin(3.92);
    var g=Math.sin(4.71);
    var h=Math.sin(5.50);
    var i=Math.cos(0);
    var j=Math.cos(0.76);
    var k=Math.cos(1.57);
    var l=Math.cos(2.36);
    var m=Math.cos(3.14);
    var n=Math.cos(3.92);
    var o=Math.cos(4.71);
    var p=Math.cos(5.50);
    var translatestring="translate("+xx+","+yx+")";
    console.log(translatestring);
    var transformstring=translatestring;
    console.log(transformstring);
    var pointstring=x*i+","+x*a+" "+ y*j+","+y*b+" "+ x*k+","+x*c+" "+ y*l+","+y*d+" "+ x*m+","+x*e+" "+ y*n+","+y*f+" "+ x*o+","+x*g+" "+ y*p+","+y*h;
    console.log(pointstring);
    Spoints.setAttribute("transform",transformstring);
    Spoints.setAttribute("points",pointstring);
}
window.Starsize=Starsize;

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtEQUEwQyxnQ0FBZ0M7QUFDMUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnRUFBd0Qsa0JBQWtCO0FBQzFFO0FBQ0EseURBQWlELGNBQWM7QUFDL0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEQUF5QyxpQ0FBaUM7QUFDMUUsd0hBQWdILG1CQUFtQixFQUFFO0FBQ3JJO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7OztBQUdBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUIiLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL2luZGV4LmpzXCIpO1xuIiwiLy9UaGlzIGlzIHRoZSBlbnRyeSBwb2ludCBKYXZhU2NyaXB0IGZpbGUuXHJcbi8vV2VicGFjayB3aWxsIGxvb2sgYXQgdGhpcyBmaWxlIGZpcnN0LCBhbmQgdGhlbiBjaGVja1xyXG4vL3doYXQgZmlsZXMgYXJlIGxpbmtlZCB0byBpdC5cclxuY29uc29sZS5sb2coXCJIZWxsbyB3b3JsZFwiKTtcclxuZnVuY3Rpb24gdXBkYXRlKCl7XHJcbiAgICBsZXQgeG09ZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiN4YXhpc1wiKTtcclxuICAgIGxldCB5bT1kb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3lheGlzXCIpO1xyXG4gICAgbGV0IHNsaWRlcj1kb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2FuZ2xlXCIpO1xyXG4gICAgbGV0IHo9ZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNjb2xsZWN0aW9uXCIpO1xyXG4gICAgdmFyIGE9TnVtYmVyKHhtLnZhbHVlKTtcclxuICAgIHZhciBiPU51bWJlcih5bS52YWx1ZSk7XHJcbiAgICB2YXIgYz1OdW1iZXIoc2xpZGVyLnZhbHVlKTtcclxuICAgIHZhciB0cmFuc2xhdGVzdHJpbmc9XCJ0cmFuc2xhdGUoXCIrYStcIixcIitiK1wiKVwiO1xyXG4gICAgY29uc29sZS5sb2codHJhbnNsYXRlc3RyaW5nKTtcclxuICAgIHZhciByb3RhdGVzdHJpbmc9XCJyb3RhdGUoXCIrYytcIilcIjtcclxuICAgIGNvbnNvbGUubG9nKHJvdGF0ZXN0cmluZyk7XHJcbiAgICB2YXIgdHJhbnNmb3Jtc3RyaW5nPXRyYW5zbGF0ZXN0cmluZytcIiBcIityb3RhdGVzdHJpbmc7XHJcbiAgICBjb25zb2xlLmxvZyh0cmFuc2Zvcm1zdHJpbmcpO1xyXG4gICAgei5zZXRBdHRyaWJ1dGUoXCJ0cmFuc2Zvcm1cIix0cmFuc2Zvcm1zdHJpbmcpO1xyXG4gICAgfVxyXG53aW5kb3cudXBkYXRlPXVwZGF0ZTtcclxuZnVuY3Rpb24gU3RhcnNpemUoKXtcclxuICAgIGxldCBTcG9pbnRzPWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjZ3JvdXBcIik7XHJcbiAgICBsZXQgb3I9ZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNvdXRlclJcIik7XHJcbiAgICBsZXQgaXI9ZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNpbm5lclJcIik7XHJcbiAgICBsZXQgeG09ZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiN4YXhcIik7XHJcbiAgICBsZXQgeW09ZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiN5YXhcIik7XHJcbiAgICB2YXIgeD1OdW1iZXIob3IudmFsdWUpO1xyXG4gICAgdmFyIHk9TnVtYmVyKGlyLnZhbHVlKTtcclxuICAgIHZhciB6PU51bWJlcihTcG9pbnRzKTtcclxuICAgIHZhciB4eD1OdW1iZXIoeG0udmFsdWUpO1xyXG4gICAgdmFyIHl4PU51bWJlcih5bS52YWx1ZSk7XHJcbiAgICBjb25zb2xlLmxvZyh4KTtcclxuICAgIGNvbnNvbGUubG9nKHkpO1xyXG4gICAgdmFyIGE9TWF0aC5zaW4oMCk7XHJcbiAgICB2YXIgYj1NYXRoLnNpbigwLjc2KTtcclxuICAgIHZhciBjPU1hdGguc2luKDEuNTcpO1xyXG4gICAgdmFyIGQ9TWF0aC5zaW4oMi4zNik7XHJcbiAgICB2YXIgZT1NYXRoLnNpbigzLjE0KTtcclxuICAgIHZhciBmPU1hdGguc2luKDMuOTIpO1xyXG4gICAgdmFyIGc9TWF0aC5zaW4oNC43MSk7XHJcbiAgICB2YXIgaD1NYXRoLnNpbig1LjUwKTtcclxuICAgIHZhciBpPU1hdGguY29zKDApO1xyXG4gICAgdmFyIGo9TWF0aC5jb3MoMC43Nik7XHJcbiAgICB2YXIgaz1NYXRoLmNvcygxLjU3KTtcclxuICAgIHZhciBsPU1hdGguY29zKDIuMzYpO1xyXG4gICAgdmFyIG09TWF0aC5jb3MoMy4xNCk7XHJcbiAgICB2YXIgbj1NYXRoLmNvcygzLjkyKTtcclxuICAgIHZhciBvPU1hdGguY29zKDQuNzEpO1xyXG4gICAgdmFyIHA9TWF0aC5jb3MoNS41MCk7XHJcbiAgICB2YXIgdHJhbnNsYXRlc3RyaW5nPVwidHJhbnNsYXRlKFwiK3h4K1wiLFwiK3l4K1wiKVwiO1xyXG4gICAgY29uc29sZS5sb2codHJhbnNsYXRlc3RyaW5nKTtcclxuICAgIHZhciB0cmFuc2Zvcm1zdHJpbmc9dHJhbnNsYXRlc3RyaW5nO1xyXG4gICAgY29uc29sZS5sb2codHJhbnNmb3Jtc3RyaW5nKTtcclxuICAgIHZhciBwb2ludHN0cmluZz14KmkrXCIsXCIreCphK1wiIFwiKyB5KmorXCIsXCIreSpiK1wiIFwiKyB4KmsrXCIsXCIreCpjK1wiIFwiKyB5KmwrXCIsXCIreSpkK1wiIFwiKyB4Km0rXCIsXCIreCplK1wiIFwiKyB5Km4rXCIsXCIreSpmK1wiIFwiKyB4Km8rXCIsXCIreCpnK1wiIFwiKyB5KnArXCIsXCIreSpoO1xyXG4gICAgY29uc29sZS5sb2cocG9pbnRzdHJpbmcpO1xyXG4gICAgU3BvaW50cy5zZXRBdHRyaWJ1dGUoXCJ0cmFuc2Zvcm1cIix0cmFuc2Zvcm1zdHJpbmcpO1xyXG4gICAgU3BvaW50cy5zZXRBdHRyaWJ1dGUoXCJwb2ludHNcIixwb2ludHN0cmluZyk7XHJcbn1cclxud2luZG93LlN0YXJzaXplPVN0YXJzaXplOyJdLCJzb3VyY2VSb290IjoiIn0=