//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");
function update(){
    let xm=document.querySelector("#xaxis");
    let ym=document.querySelector("#yaxis");
    let slider=document.querySelector("#angle");
    let z=document.querySelector("#collection");
    var a=Number(xm.value);
    var b=Number(ym.value);
    var c=Number(slider.value);
    var translatestring="translate("+a+","+b+")";
    console.log(translatestring);
    var rotatestring="rotate("+c+")";
    console.log(rotatestring);
    var transformstring=translatestring+" "+rotatestring;
    console.log(transformstring);
    z.setAttribute("transform",transformstring);
    }
window.update=update;
function Starsize(){
    let Spoints=document.querySelector("#group");
    let or=document.querySelector("#outerR");
    let ir=document.querySelector("#innerR");
    let xm=document.querySelector("#xax");
    let ym=document.querySelector("#yax");
    var x=Number(or.value);
    var y=Number(ir.value);
    var z=Number(Spoints);
    var xx=Number(xm.value);
    var yx=Number(ym.value);
    console.log(x);
    console.log(y);
    var a=Math.sin(0);
    var b=Math.sin(0.76);
    var c=Math.sin(1.57);
    var d=Math.sin(2.36);
    var e=Math.sin(3.14);
    var f=Math.sin(3.92);
    var g=Math.sin(4.71);
    var h=Math.sin(5.50);
    var i=Math.cos(0);
    var j=Math.cos(0.76);
    var k=Math.cos(1.57);
    var l=Math.cos(2.36);
    var m=Math.cos(3.14);
    var n=Math.cos(3.92);
    var o=Math.cos(4.71);
    var p=Math.cos(5.50);
    var translatestring="translate("+xx+","+yx+")";
    console.log(translatestring);
    var transformstring=translatestring;
    console.log(transformstring);
    var pointstring=x*i+","+x*a+" "+ y*j+","+y*b+" "+ x*k+","+x*c+" "+ y*l+","+y*d+" "+ x*m+","+x*e+" "+ y*n+","+y*f+" "+ x*o+","+x*g+" "+ y*p+","+y*h;
    console.log(pointstring);
    Spoints.setAttribute("transform",transformstring);
    Spoints.setAttribute("points",pointstring);
}
window.Starsize=Starsize;